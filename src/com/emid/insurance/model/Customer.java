package com.emid.insurance.model;

import java.util.Date;

public class Customer {

	private int id;
	private String name;	
	private String gender;
	private int age;
	private Date policyStartDate;
	private CustomerHealth customerHealth;
	private CustomerHabit customerHabit;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getPolicyStartDate() {
		return policyStartDate;
	}
	public void setPolicyStartDate(Date policyStartDate) {
		this.policyStartDate = policyStartDate;
	}
	public CustomerHealth getCustomerHealth() {
		return customerHealth;
	}
	public void setCustomerHealth(CustomerHealth customerHealth) {
		this.customerHealth = customerHealth;
	}
	public CustomerHabit getCustomerHabit() {
		return customerHabit;
	}
	public void setCustomerHabit(CustomerHabit customerHabit) {
		this.customerHabit = customerHabit;
	}
	
	
}
