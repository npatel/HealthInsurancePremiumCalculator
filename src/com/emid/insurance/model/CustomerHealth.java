package com.emid.insurance.model;

public class CustomerHealth {

		private int id;
		private String hypertension;
		private String bloodPressure;
		private String bloodSugar;
		private String overweight;
		private int  idCustomer;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getHypertension() {
			return hypertension;
		}
		public void setHypertension(String hypertension) {
			this.hypertension = hypertension;
		}
		public String getBloodPressure() {
			return bloodPressure;
		}
		public void setBloodPressure(String bloodPressure) {
			this.bloodPressure = bloodPressure;
		}
		public String getBloodSugar() {
			return bloodSugar;
		}
		public void setBloodSugar(String bloodSugar) {
			this.bloodSugar = bloodSugar;
		}
		public String getOverweight() {
			return overweight;
		}
		public void setOverweight(String overweight) {
			this.overweight = overweight;
		}
		public int getIdCustomer() {
			return idCustomer;
		}
		public void setIdCustomer(int idCustomer) {
			this.idCustomer = idCustomer;
		}
}
