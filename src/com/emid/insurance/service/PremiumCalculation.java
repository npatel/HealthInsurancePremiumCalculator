package com.emid.insurance.service;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.emid.insurance.model.Customer;
import com.emid.insurance.model.CustomerHabit;
import com.emid.insurance.model.CustomerHealth;

/**
 * Health insurance premium calculation
 * @author nirmal
 * 
 */
public class PremiumCalculation {
       
	   public static int basePremiun = 5000;
	   
	   /**
	    * Total premium calculation based on all the criteria(age, gender, health conditions, habits).
	    * @param customer
	    * return void
	    * @throws Exception -- If customer is null or customer name is null
	    */       
	   public static int premiumCalculator(Customer customer) throws Exception {
		   
		   String cutomerPremium;
		   
	   	   if(null == customer) {
	   		   throw new Exception("Customer does not exist.");
	   	   }
	   	   
	   	   if(customer != null && null == customer.getName() ){
	   		   throw new Exception("Customer does not exist."); 
	   	   }
	   	   
	       int premiumBasedonAge = premiumBasedOnAge(customer.getPolicyStartDate(), customer.getAge());
	       //System.out.println("Extra premium for age " + premiumBasedonAge);
	       int premiumBasedOnGender = premiumBasedOnGender(customer.getGender());
	       //System.out.println("EXtra premium for gender " + premiumBasedOnGender);
	       int premiumBasedOnHealth = premiumBasedOnhealthCondition(customer.getCustomerHealth());
	       //System.out.println("Extra premium for health "+ premiumBasedOnHealth);
	       int premiumBasedOnHabit = premiumBasedOnHabits(customer.getCustomerHabit());
	       //System.out.println("Extra premium for habit "+premiumBasedOnHabit);
	       
	       int finalPremium = basePremiun + premiumBasedonAge + premiumBasedOnGender + premiumBasedOnHealth + premiumBasedOnHabit;
	       
	       // Convert Indian currency format
	       Locale indian = new Locale("en", "IN");
	       NumberFormat indianFormat = NumberFormat.getCurrencyInstance(indian);
	       indianFormat.setMaximumFractionDigits(0);
	     
	       if(null == customer.getGender()){
	    	   cutomerPremium = "Health Insurance Premium for "+ customer.getName()+": " + indianFormat.format(finalPremium);
	       }else if(customer.getGender().equalsIgnoreCase("male")){
	    	   cutomerPremium = "Health Insurance Premium for Mr. "+ customer.getName()+": " + indianFormat.format(finalPremium);
	       }else if(customer.getGender().equalsIgnoreCase("female")) {
	    	   cutomerPremium = "Health Insurance Premium for Mrs. "+ customer.getName()+": " + indianFormat.format(finalPremium);
	       }else {
	    	   cutomerPremium = "Health Insurance Premium for "+ customer.getName()+": " + indianFormat.format(finalPremium);
	       }
	       System.out.println(cutomerPremium);
	       return finalPremium;
	   }
	
	   /**
	    * Premium calculation based on the age. 
	    * 	If age is between 18 and 40 then it is 10% on basic premium.
	    * 	If age is more then 40 years then 20% increase every 5 years.
	    * @param policyDate
	    * @param age
	    * @return int  -- Extra Premium
	    */
	   public static int premiumBasedOnAge(Date policyDate, int age) {
	           
	       int premiumAfterAgeValidate = 0;                   
	       if(age > 17 && age < 41) {
	               premiumAfterAgeValidate = basePremiun / 100 * 10 ;
	       }else if(age > 40) {                        
	               // every 5 years
	               DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	           int d1 = Integer.parseInt(formatter.format(policyDate));
	           int d2 = Integer.parseInt(formatter.format(new Date()));
	           int year = (d2-d1)/10000;
	          
	           int numOfYear = year / 5;
	           if(numOfYear < 1) {
	                   premiumAfterAgeValidate = basePremiun / 100 * 20 ;
	           }else {
	                   premiumAfterAgeValidate = compoundInterest(numOfYear+1);
	           }
	       }               
	       return premiumAfterAgeValidate;
	   }
	   
	   /**
	    * Calculate premium based on gender. If Male then extra 2% premium
	    * @param gender
	    * @return int -- Extra Premium
	    */
	   public static int premiumBasedOnGender(String gender) {
	           
	           int premiumAfterGenderValidate = 0;
	           if(null == gender){
	        	   return premiumAfterGenderValidate;
	           }
	           
	           if(gender.equalsIgnoreCase("male")) {
	                   premiumAfterGenderValidate = basePremiun / 100 * 2 ;
	           }
	           return premiumAfterGenderValidate;
	   }
	   
	   /**
	    * Premium based on the health condition. If person has any health issue then extra 1% premium
	    * @param customerHealth
	    * @return int -- Extra Premium
	    */
	   public static int premiumBasedOnhealthCondition(CustomerHealth customerHealth) {
	           
	       int premiumAfterHealthValidate = 0;        
	       
	       if(customerHealth != null && 
	                       ((customerHealth.getBloodPressure() != null && customerHealth.getBloodPressure().equalsIgnoreCase("yes")) ||
	                       (customerHealth.getBloodSugar() != null && customerHealth.getBloodSugar().equalsIgnoreCase("yes")) ||
	                       (customerHealth.getHypertension() != null && customerHealth.getHypertension().equalsIgnoreCase("yes"))||                                
	                       (customerHealth.getOverweight() != null && customerHealth.getOverweight().equalsIgnoreCase("yes")))) {
	               premiumAfterHealthValidate =  basePremiun / 100 * 1;
	       }
	       return premiumAfterHealthValidate;
	   }
	
	   /**
	    * Calculate premium based on the Habits.
	    * 		If person has both good and bad habit then no extra premium
	    * 		If person has only good habit then reduce 3% on base premium
	    * 		If person has only bad habit then add 3% on base premium
	    * @param customerHabit
	    * @return int -- Extra Premium
	    */
	   public static int premiumBasedOnHabits(CustomerHabit customerHabit) {
	           
	       int premiumAfterHabit = 0;  
	       
	       if(null == customerHabit){
	    	   return 0;
	       }
	       
	       if(customerHabit.getDailyExercise() != null && customerHabit.getDailyExercise().equalsIgnoreCase("yes") &&
	    		           ((customerHabit.getAlcohol() != null && customerHabit.getAlcohol().equalsIgnoreCase("yes")) ||
	                       (customerHabit.getDrugs() != null && customerHabit.getDrugs().equalsIgnoreCase("yes"))||                                
	                       (customerHabit.getSmoking() != null && customerHabit.getSmoking().equalsIgnoreCase("yes")))){
	    	   return premiumAfterHabit;
	       }else if(customerHabit.getDailyExercise() != null && customerHabit.getDailyExercise().equalsIgnoreCase("yes")) {
	    	   premiumAfterHabit = basePremiun / 100 * 3;
	           premiumAfterHabit = premiumAfterHabit - (2 *premiumAfterHabit); 
	       }else if((customerHabit.getAlcohol() != null && customerHabit.getAlcohol().equalsIgnoreCase("yes")) ||
	                       (customerHabit.getDrugs() != null && customerHabit.getDrugs().equalsIgnoreCase("yes"))||                                
	                       (customerHabit.getSmoking() != null && customerHabit.getSmoking().equalsIgnoreCase("yes"))) {
	    	   premiumAfterHabit = basePremiun / 100 * 3 ;
	               
	       }
	       return premiumAfterHabit;
	   }
	   
	   
	   /**
	    * Calculate compound interest for every 5 years
	    * @param time -- Number of 5 years from policy start date
	    * @return int -- compound interest
	    */
	   public static int compoundInterest(int time){                
	       double amount=0;
	       double rate = 10;
	       int ci = 0;
	       rate=(1+rate/100);
	       rate=java.lang.Math.pow(rate,time);
	       amount=basePremiun*rate;
	       System.out.println("amount="+amount);
	       ci=(int) (amount-basePremiun);
	       System.out.println("compound interest="+ci);
	       return ci;
	   }               
}