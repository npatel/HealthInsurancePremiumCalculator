package com.emid.insurance.service;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.emid.insurance.model.Customer;
import com.emid.insurance.model.CustomerHabit;
import com.emid.insurance.model.CustomerHealth;

public class PremiumCalculationHandler {
	
	public static int basePremiun = 5000;
	
	public static void main(String[] args) {
		
		
		
		CustomerHealth customerHealth = new CustomerHealth();
		customerHealth.setBloodPressure("YES");
		customerHealth.setBloodSugar("NO");
		customerHealth.setHypertension("NO");
		customerHealth.setOverweight("NO");
		
		CustomerHabit customerHabit = new CustomerHabit();
		customerHabit.setAlcohol("NO");
		customerHabit.setDailyExercise("YES");
		customerHabit.setDrugs("NO");
		customerHabit.setSmoking("NO");
		
		Customer customer = new Customer();
		customer.setAge(50);
		customer.setGender("M");
		customer.setName("Abc");
		customer.setPolicyStartDate(new Date());
		customer.setCustomerHabit(customerHabit);
		customer.setCustomerHealth(customerHealth);
		
		
		int premiumBasedonAge = premiumBasedOnAge(customer.getPolicyStartDate(), customer.getAge());
		System.out.println(premiumBasedonAge);
		int premiumBasedOnGender = premiumBasedOnGender("M");
		System.out.println(premiumBasedOnGender);
		int premiumBasedOnHealth = premiumBasedOnhealthCondition(customerHealth);
		System.out.println(premiumBasedOnHealth);
		int premiumBasedOnHabit = premiumBasedOnHabits(customerHabit);
		System.out.println(premiumBasedOnHabit);
			
		
		int finalPremium = basePremiun + premiumBasedonAge + premiumBasedOnGender + premiumBasedOnHealth + premiumBasedOnHabit;
		
		Locale indian = new Locale("en", "IN");
		NumberFormat indianFormat = NumberFormat.getCurrencyInstance(indian);
		System.out.println("Indian: " + indianFormat.format(finalPremium));
		 
		System.out.println("Health Insurance Premium for " + indianFormat.format(finalPremium));
		
	}

	public static int premiumBasedOnAge(Date policyDate, int age) {
		
		int premiumAfterAgeValidate = basePremiun;	
		
		//if age  is between 18-25, 10% extra premium
		if(age > 17 && age < 41) {
			premiumAfterAgeValidate = basePremiun / 100 * 10 ;
		}else if(age > 40) {	
			
			
			// every 5 years
			DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    int d1 = Integer.parseInt(formatter.format(policyDate));
		    int d2 = Integer.parseInt(formatter.format(new Date()));
		    int year = (d2-d1)/10000;
		   
		    int numOfYear = year / 5;
		    if(numOfYear < 1) {
		    	premiumAfterAgeValidate = basePremiun / 100 * 20 ;
		    }else {
		    	//need to find the  every five year
		    	premiumAfterAgeValidate = basePremiun / 100 * 20 ;
		    }
		}
		
		
	    
	    
	    
		return premiumAfterAgeValidate;
	}
	
	public static int premiumBasedOnGender(String gender) {
		
		int premiumAfterGenderValidate = 0;	
	
		if(gender.equalsIgnoreCase("m")) {
			premiumAfterGenderValidate = basePremiun / 100 * 2 ;
		}
		return premiumAfterGenderValidate;
	}
	
	public static int premiumBasedOnhealthCondition(CustomerHealth customerHealth) {
		
		int premiumAfterHealthValidate = 0;	
		
		if(customerHealth != null && 
				((customerHealth.getBloodPressure() != null && customerHealth.getBloodPressure().equalsIgnoreCase("yes")) ||
				(customerHealth.getBloodSugar() != null && customerHealth.getBloodSugar().equalsIgnoreCase("yes")) ||
				(customerHealth.getHypertension() != null && customerHealth.getHypertension().equalsIgnoreCase("yes"))||				
				(customerHealth.getOverweight() != null && customerHealth.getOverweight().equalsIgnoreCase("yes")))) {
			premiumAfterHealthValidate =  basePremiun / 100 * 1;
		}
		return premiumAfterHealthValidate;
	}

	
	public static int premiumBasedOnHabits(CustomerHabit customerHabit) {
		
		int premiumAfterHabit = 0;	
		
		if(customerHabit != null && 
				customerHabit.getDailyExercise() != null && customerHabit.getDailyExercise().equalsIgnoreCase("yes")) {
			premiumAfterHabit = basePremiun / 100 *3 ;
		}else if(customerHabit != null && 
				((customerHabit.getAlcohol() != null && customerHabit.getAlcohol().equalsIgnoreCase("yes")) ||
				(customerHabit.getDrugs() != null && customerHabit.getDrugs().equalsIgnoreCase("yes"))||				
				(customerHabit.getSmoking() != null && customerHabit.getSmoking().equalsIgnoreCase("yes")))) {
			premiumAfterHabit = basePremiun / 100 * 3;
			premiumAfterHabit = premiumAfterHabit - (2 *premiumAfterHabit);
			
		}
		return premiumAfterHabit;
	}

		
}
