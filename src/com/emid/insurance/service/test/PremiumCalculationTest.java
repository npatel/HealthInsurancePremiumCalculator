package com.emid.insurance.service.test;


import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import com.emid.insurance.model.Customer;
import com.emid.insurance.model.CustomerHabit;
import com.emid.insurance.model.CustomerHealth;
import com.emid.insurance.service.PremiumCalculation;

/**
 * Test class for Health Insurance Premium Calculator
 * @author nirmal
 *
 */
public class PremiumCalculationTest {

       @Test
       public void testPremiumCalculator() throws Exception {
               
               CustomerHealth customerHealth = new CustomerHealth();
               customerHealth.setBloodPressure("YES");
               customerHealth.setBloodSugar("NO");
               customerHealth.setHypertension("NO");
               customerHealth.setOverweight("NO");
               
               CustomerHabit customerHabit = new CustomerHabit();
               customerHabit.setAlcohol("NO");
               customerHabit.setDailyExercise("YES");
               customerHabit.setDrugs("NO");
               customerHabit.setSmoking("NO");
               
               Customer customer = new Customer();
               customer.setAge(30);
               customer.setGender("male");
               customer.setName("Nirmal");
               customer.setPolicyStartDate(new Date());
               customer.setCustomerHabit(customerHabit);
               customer.setCustomerHealth(customerHealth);
               //Give the expected premium
               assertEquals(5500,PremiumCalculation.premiumCalculator(customer));
       }

}
